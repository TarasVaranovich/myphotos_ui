var photosUserKeyInput = document.getElementById("photosUserKey");
var photosUserIDInput = document.getElementById("photosUserID");
var userNamePhotostText = document.getElementById("userNamePhotos");
var exitButton = document.getElementById("exitBtn");
var previousButton = document.getElementById("previousPhotoBtn");
var nextButton = document.getElementById("nextPhotoBtn");
var addButton = document.getElementById("addPhotoBtn");
var deleteButton = document.getElementById("deletePhotoBtn");
var createButton = document.getElementById("createDocBtn");
var getDocsButton = document.getElementById("getAllDocsBtn");

var photoImages = document.getElementsByClassName("photoImage");
var photoNames = document.getElementsByClassName("photoName");
var photoDates = document.getElementsByClassName("photoDate");
var photoDescriptions = document.getElementsByClassName("photoDesc");
var photoIDs = document.getElementsByClassName("photoId");
var photoCheckBoxes =  Array.prototype.slice.call(document.getElementsByClassName("photoCheckBox"));
var photoCheckBoxesLabels =  Array.prototype.slice.call(document.getElementsByClassName("photoCheckBoxLabel"));
var formModal = document.getElementById("formModal");
var createDocForm = document.getElementById("docNameForm");
var createDocOKButton = document.getElementById("createDocOKBtn");
var createDocCancelButton = document.getElementById("createDocCancelBtn");
var createDocNameTextBox = document.getElementById("pdfDocName");
var createDocSaveCheckBox = document.getElementById("saveDocCheckBox");

var createDocButtonSwitcher = true;
var deletePhotosButtonSwitcher = true;
var checkBoxesVisible = false;
var photoIDsArr = [];
var addPhotosTransition = false;

var deletePhotosForm = document.getElementById("deletePhotosForm");
var deletePhotosYesBtn = document.getElementById("deletePhotosYesBtn");
var deletePhotosNoBtn = document.getElementById("deletePhotosNoBtn");
var previewPhotoForm = document.getElementById("previewPhotoForm");
var descriptionForm = document.getElementById("descriptionForm");

var photosCursor = 0;

var popUpOpenedInput = document.getElementById("popUpOpened");
var waitAnimation = document.getElementById("waitAnimation");
var commonAlert = document.getElementById("commonAlert");


window.onload = function() {
	console.log("Load...");
	var xhr = new XMLHttpRequest();
    xhr.open("post","/myphotos/photos.htm/usr.htm");
    xhr.setRequestHeader('Content-Type', 'text/html');
    xhr.send(null);
    xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
       if(xhr.status == 200) {
       		var stringValue = xhr.responseText;
       		photosUserKeyInput.value = JSON.parse(stringValue).token;
       		photosUserIDInput.value = JSON.parse(stringValue).uuid;

	       		for(var i = 0; i < photoImages.length; i++) {
	       			getImage(i, i);
	       		}
           }
        }
    };

    console.log("Load photos...");

    createDocForm.style.display = "none"; // NONE BY DEFAULT
    deletePhotosForm.style.display="none"; // NONE BY DEFAULT
		formModal.style.display = "none"; // NONE BY DEFAULT
		previewPhotoForm.style.display = "none"; // NONE BY DEFAULT
		descriptionForm.style.display = "none";  // NONE BY DEFAULT
		waitAnimation.style.display = "none";
    previousButton.disabled = true;
    popUpOpenedInput.value = "";
		commonAlert.style.display = "none";
    for (var k = 0; k < photoCheckBoxes.length; k++) {
    	photoCheckBoxes[k].style.display = "none"; // NONE BY DEFAULT
			photoCheckBoxesLabels[k].style.display = "none";
    	photoCheckBoxes[k].addEventListener("click", function(event) {
    		checkBoxClick(this);
    	});
    }

}

window.onbeforeunload = function(){  // for page close
	if(popUpOpenedInput.value == 0) {
		deleteDocument();
	}
}

window.onunload = function(){  // for browser "BACK" button
	if(popUpOpenedInput.value == 0) {
		deleteDocument();
	}
}

window.onfocus = function() {

	if(addPhotosTransition) {
		console.log("From add...");
		refreshPhotos();
		addPhotosTransition = false;
	}
}

exitButton.addEventListener("click", function(event) {
	if(popUpOpenedInput.value == 0) {
		deleteDocument();
	}
	exitFromPhotos();
	//window.location.href = "/myphotos/welcome.htm";

});

addButton.addEventListener("click", function(event) {
	addPhotosTransition = true;
	window.location.href = "/myphotos/addphotos.htm?uuid="
								+ photosUserIDInput.value
												+ "&key="
									+ photosUserKeyInput.value;
});

nextButton.addEventListener("click", function(event) {
	photosCursor = photosCursor + 1;
	for(var i = 0; i < photoImages.length; i++) {
		getImage(i, (photosCursor + i));
	}
	previousButton.disabled = false;
	console.log("->next");
});

previousButton.addEventListener("click", function(event) {

	photosCursor = photosCursor - 1;
	nextButton.disabled = false;
	if(photosCursor >= 0 ) {
		if(photosCursor == 0) {
			previousButton.disabled = true;
		}
		for(var k = 0; k < photoImages.length; k++) {
			photoImages[k].style.display = 'inline-block';
			photoNames[k].style.display = 'inline-block';
			photoDates[k].style.display = 'inline-block';
			photoDescriptions[k].style.display = 'inline-block';
			photoIDs[k].style.display = 'inline-block';
			if(checkBoxesVisible) {
				photoCheckBoxes[k].style.display = 'inline-block'; //////
			}
		}
		for(var i = 0; i < photoImages.length; i++) {
			getImage(i, (photosCursor + i));
		}
	}
});


deleteButton.addEventListener("click", function(event) {
	if(deletePhotosButtonSwitcher) {
		for (var k = 0; k < photoCheckBoxes.length; k++) {
	    	photoCheckBoxes[k].style.display = "inline-block";
				photoCheckBoxesLabels[k].style.display = "inline-block";
	    }
		checkBoxesVisible = true;
		this.innerHTML = "Delete";
		deletePhotosButtonSwitcher = false;
	} else {

		deletePhotosButtonSwitcher = true;

		if(photoIDsArr.length > 0) {
			deletePhotosForm.style.display = "block";
			formModal.style.display = "block";
		} else {
			alert("No photos checked!");
			setPreventState();
		}
		this.innerHTML = "Delete photos";
	}
});

deletePhotosYesBtn.addEventListener("click", function(event) {

	deletePhotos(photoIDsArr);
	setPreventState();

});

deletePhotosNoBtn.addEventListener("click", function(event) {

	setPreventState();
});

getDocsButton.addEventListener("click", function(event) {
	window.location.href = "/myphotos/documents.htm?uuid="
								+ photosUserIDInput.value
								+ "&key="
								+ photosUserKeyInput.value;
});

createButton.addEventListener("click", function(event) {
	if(createDocButtonSwitcher) {
		for (var k = 0; k < photoCheckBoxes.length; k++) {
	    	photoCheckBoxes[k].style.display = "inline-block";
				photoCheckBoxesLabels[k].style.display = "inline-block";
	    }
		checkBoxesVisible = true;
		this.innerHTML = "Create";
		createDocButtonSwitcher = false;

	} else {

		createDocButtonSwitcher = true;

		if(photoIDsArr.length > 0) {
			createDocForm.style.display = "block";
			formModal.style.display = "block";
			createDocSaveCheckBox.style.display = "none";

		} else {
			alert("No photos checked!");
			setPreventState();
		}
		this.innerHTML = "Create Doc";
	}


});

createDocOKButton.addEventListener("click", function(event) {
	if(createDocNameTextBox.value !="") {
		sendPDFdata(photoIDsArr,createDocNameTextBox.value, createDocSaveCheckBox.checked);
		setPreventState();
	} else {
		alert("Enter doc name.");
	}

});

createDocCancelButton.addEventListener("click", function(event) {
	setPreventState();

});

function checkBoxClick(checkbox) {

	if(checkbox.checked) {
		photoIDsArr.push(photoIDs[photoCheckBoxes.indexOf(checkbox)].value);
		console.log("->push:" + photoIDs[photoCheckBoxes.indexOf(checkbox)].value);
	} else {
		var element = photoIDs[photoCheckBoxes.indexOf(checkbox)].value;
		deleteElement(photoIDsArr, element);
		console.log("->pop:" + photoIDs[photoCheckBoxes.indexOf(checkbox)].value);
	}
}

function validateCheckBox(k ,id) {

	console.log("INDEX:" + photoIDsArr.indexOf(id));

	if(elementExists(photoIDsArr, id)) {

		photoCheckBoxes[k].checked = true;
		console.log("->checked:" + k + ":" + id);

	} else {

		photoCheckBoxes[k].checked = false;
		console.log("->unchecked" + k + ":" + id);
	}
}

function deleteElement(array, element) {
	for(var p = 0; p < array.length; p++) {
		if(array[p] == element) {
			array.splice(p,1);
			console.log("Element deleted");
		}
	}
}

function elementExists(array, element) {
	for(var p = 0; p < array.length; p++) {

		console.log("Compare:" + array[p] + ":" + parseInt(element));

		if(array[p] == parseInt(element)) {
			console.log("compare true");
			return true;

			break;
		} else if (p == (array.length-1)){
			return false;
		}
	}
}
function getImage(z, k){

	var xhrPhoto = new XMLHttpRequest();
	xhrPhoto.open("get","/myphotos/photos.htm/getphotos.htm?uuid="
										+ photosUserIDInput.value
										+ "&key="
										+ photosUserKeyInput.value);
	xhrPhoto.setRequestHeader('Content-Type', 'application/json');
	xhrPhoto.setRequestHeader('photonumber', k);
	xhrPhoto.send();
	xhrPhoto.onreadystatechange = function() {
    if (xhrPhoto.readyState == 4) {
       if(xhrPhoto.status == 202) {
         var stringValuePhoto = xhrPhoto.responseText;
         if(stringValuePhoto!= "") {
	         photoImages[z].src = JSON.parse(stringValuePhoto).photobin;
	         photoImages[z].alt = JSON.parse(stringValuePhoto).photoname;
	         photoNames[z].innerHTML = JSON.parse(stringValuePhoto).photoname;
	         photoDates[z].innerHTML = JSON.parse(stringValuePhoto).photodate;
	         photoDescriptions[z].innerHTML = JSON.parse(stringValuePhoto).photodesc;
	         var idCheck = JSON.parse(stringValuePhoto).photoid;
	         photoIDs[z].value = idCheck;
	         validateCheckBox(z,idCheck);
         } else {
        	 console.log("display none");
        	 photoImages[z].style.display = 'none';
        	 photoNames[z].style.display = 'none';
        	 photoDates[z].style.display = 'none';
        	 photoDescriptions[z].style.display = 'none';
        	 photoIDs[z].style.display = 'none';
        	 photoCheckBoxes[z].style.display = 'none';
					 photoCheckBoxesLabels[z].style.display = 'none';
        	 nextButton.disabled = true;
         }
        }
      }
    };
}
function sendPDFdata(IDS, pdfName, saveInd) {
	var pdfObj = new Object();
	pdfObj.photoIDS = IDS;
	pdfObj.docName = pdfName;
	pdfObj.saveDoc = saveInd;
	var pdfData = JSON.stringify(pdfObj);
	var xhrPDF = new XMLHttpRequest();
	xhrPDF.open("post","/myphotos/photos.htm/createdoc.htm?uuid="
										+ photosUserIDInput.value
										+ "&key="
										+ photosUserKeyInput.value);
	xhrPDF.setRequestHeader('Content-Type', 'application/json');
	xhrPDF.send(pdfData);
	xhrPDF.onreadystatechange = function() {
    if (xhrPDF.readyState == 4) {
       if(xhrPDF.status == 200) {
         var stringValuePDF = xhrPDF.responseText;

	            if(!window.open(stringValuePDF)) {
	            	popUpOpenedInput.value = 0;
	            }
           }
        }
    };
}

function deletePhotos(IDS) {
	var deleteObj = new Object();
	deleteObj.photoIDS = IDS;
	var deleteData = JSON.stringify(deleteObj);
	var xhrDelete = new XMLHttpRequest();
	xhrDelete.open("post","/myphotos/photos.htm/deletephotos.htm?uuid="
										+ photosUserIDInput.value
										+ "&key="
										+ photosUserKeyInput.value);
	xhrDelete.setRequestHeader('Content-Type', 'application/json');
	xhrDelete.send(deleteData);
	xhrDelete.onreadystatechange = function() {
    if (xhrDelete.readyState == 4) {
       if(xhrDelete.status == 200) {
         var stringValueDelete = xhrDelete.responseText;

         			if(stringValueDelete == "true") {
         				refreshPhotos();
         			}
           }
        }
    };
}

function setPreventState() {
	photoIDsArr.length = 0;
	createDocNameTextBox.value = null;
	createDocSaveCheckBox.checked = false;
	createDocButtonSwitcher = true;
	checkBoxesVisible = false;
	createDocForm.style.display = "none";
	deletePhotosForm.style.display="none";
	formModal.style.display = "none";
	for (var k = 0; k < photoCheckBoxes.length; k++) {
    	photoCheckBoxes[k].style.display = "none";
			photoCheckBoxesLabels[k].style.display = "none";
    }
	createButton.innerHTML = "Create Doc";
	deleteButton.innerHTML = "Delete Photo";
}

function refreshPhotos() {
	nextButton.disabled = false;
	for(var i = 0; i < photoImages.length; i++) {
		photoImages[i].style.display = 'inline-block';
   	 	photoNames[i].style.display = 'inline-block';
   	 	photoDates[i].style.display = 'inline-block';
   	 	photoDescriptions[i].style.display = 'inline-block';
   	 	photoIDs[i].style.display = 'inline-block';
 		getImage(i, (photosCursor + i));
 	}
}

function exitFromPhotos() {
	console.log("Exit...");
	var xhrExitFromPhotos = new XMLHttpRequest();
	xhrExitFromPhotos.open("get","/myphotos/photos.htm/exitfromphotos.htm?uuid="
			+ photosUserIDInput.value
			+ "&key="
			+ photosUserKeyInput.value);
	xhrExitFromPhotos.setRequestHeader('Content-Type', 'text/html');
	xhrExitFromPhotos.send();
	xhrExitFromPhotos.onreadystatechange = function() {
    if (xhrExitFromPhotos.readyState == 4) {
       if(xhrExitFromPhotos.status == 200) {
    	    console.log("Exit");
    	    window.location.href = "/myphotos/welcome.htm";
           }
        }
    };
}
function deleteDocument() {
	console.log("Delete doc from photos branch")
	var xhrdeleteDoc = new XMLHttpRequest();
	xhrdeleteDoc.open("get","/myphotos/photos.htm/removedocument.htm?uuid="
			+ photosUserIDInput.value
			+ "&key="
			+ photosUserKeyInput.value);
	xhrdeleteDoc.setRequestHeader('Content-Type', 'text/html');
	xhrdeleteDoc.send();//null fixed
	xhrdeleteDoc.onreadystatechange = function() {
    if (xhrdeleteDoc.readyState == 4) {
       if(xhrdeleteDoc.status == 202) {
    	    console.log("Removed");
           }
        }
    };
}
