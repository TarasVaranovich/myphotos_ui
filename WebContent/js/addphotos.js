var addPhotosUserKeyInput = document.getElementById("addPhotosUserKey");
var addPhotosUserIDInput = document.getElementById("addPhotosUserID");
var exitButton = document.getElementById("exitBtn");
var sendButton = document.getElementById("sendBtn");
var cancelButton = document.getElementById("cancelBtn");
var choosePhotosBtn = document.getElementById("choosePhotos");
var photosPreviewArea = document.getElementById("addPhotosPreviewArea");
var displayPhotosOutput = document.getElementById("displayPhotos");
var progress = document.querySelector('.percent');
var editImageForm = document.getElementById("editImageForm");
// form
//var editImageNameInput = document.getElementById("editImageForm_name");
var imageNameParagraph = document.getElementById("imageName");
var imageExtensionSpan = document.getElementById("imageExtension");
var editImageDescription = document.getElementById("editImageForm_decription");
var editImageOkBtn = document.getElementById("editImageForm_OK");
var editImageCancelBtn = document.getElementById("editImageForm_Cancel");
//end form
var waitAnimation = document.getElementById("waitAnimation");
var photosArray = [];
var editLocal = "";
var commonAlert = document.getElementById("commonAlert");

window.onload = function() {
	console.log("Load...");
	editImageForm.style.display = 'none';// NONE BY DEFAULT
	sendButton.disabled = true; // TRUE BY DEFAULT
	cancelButton.disabled = true; // TRUE BY DEFAULT
	photosPreviewArea.style.display = 'none';
	waitAnimation.style.display = 'none';
	commonAlert.style.display = 'none';
	var xhr = new XMLHttpRequest();
    xhr.open("post","/myphotos/addphotos.htm/usr.htm");
    xhr.setRequestHeader('Content-Type', 'text/html');
    xhr.send(null);
    xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
       if(xhr.status == 200) {
       		var stringValue = xhr.responseText;
       		addPhotosUserKeyInput.value = JSON.parse(stringValue).token;
       		addPhotosUserIDInput.value = JSON.parse(stringValue).uuid;
           }
        }
    };
}

window.onbeforeunload = function(){
	//exitFromAddPhotos();
}

sendButton.addEventListener("click", function(event) {
		tempObjectTest(photosArray);
		if(sendImage(JSON.stringify(photosArray))) {
			setPreventState();
		} else {
			alert("Error!");
			setPreventState();
		}
});

cancelButton.addEventListener("click", function(event) {

		setPreventState();

});

exitButton.addEventListener("click", function(event) {

	exitFromAddPhotos();
	window.location.href = "/myphotos/welcome.htm";

});

choosePhotosBtn.addEventListener("change", function(event) {
	 // CLEAR ARRAY
	photosPreviewArea.style.display = 'block';
	cancelButton.disabled = false;
    var files = event.target.files;
    var output = [];
    var fk = 0;
    for(var i = 0; i < files.length; i++) {
    	var f = files[i];
        if (!f.type.match('image.*')) {
          continue;
        }
        //for progress
        progress.style.width = '0%';
        progress.textContent = '0%';
        //end for progress
        var reader = new FileReader();
        //for progress
        reader.onerror = errorHandler;
        reader.onprogress = updateProgress;
        reader.onloadstart = function(e) {
        	sendButton.disabled = true;
            document.getElementById('progress_bar').className = 'loading';
        };
        reader.onloadend = function(e) {
        	//sendButton.disabled = false;
        	unblockSendBtn(fk, files.length);
        	fk = fk + 1;
        };
      //end for progress
        reader.onload = (function(theFile) {
          return function(e) {
        	//for progress
        	progress.style.width = '100%';
            progress.textContent = '100%';
            setTimeout("document.getElementById('progress_bar').className='';", 200);//!!!  2000
            //end for progress
            var span = document.createElement('div');
						span.className = "imageContainer";
            span.innerHTML = ['<img class="thumb" src="',
            						   	 e.target.result,
                                             '" title="',
                                    escape(theFile.name),
                         '"/><p class="photo_name" id="',
                         				theFile.name,'" onclick="imageClick(this)">',
                         				editLocal,'</p>'].join('');
            //read image data in array
            var strArr = theFile.name.split('.');
            var photoObj = new Object();
            photoObj.name = strArr[0];
            photoObj.format = strArr[1];
            photoObj.binary = e.target.result;
            photoObj.description = "Photo " + strArr[0];
            photoObj.date = todayDateString();
            photosArray.push(photoObj);
            //end read image data in array
            displayPhotosOutput.insertBefore(span, null);
          };
        })(f);

        reader.readAsDataURL(f);
    }
});

editImageCancelBtn.addEventListener("click", function(event) {
	editImageForm.style.display = 'none';
});

editImageOkBtn.addEventListener("click", function(event) {
	//var predicateStr = editImageNameInput.value.split('.');
	//var currentImageName = predicateStr[0];
	var currentImageName = imageNameParagraph.innerHTML;
	var currentObj = photosArray.find(x=> x.name === currentImageName);

	//var photoName = editImageNameInput.value;
	//currentObj.name = photoName;
	var photoDescription = editImageDescription.value;
	currentObj.description = photoDescription;
	var originalDate = document.getElementById("datepicker").value;
	currentObj.date = originalDate.toString();
	editImageForm.style.display = 'none';
});

function unblockSendBtn(counter, count){

	console.log("I:" + counter + "LEN:" + count);
	if(counter == (count - 1)) {
		sendButton.disabled = false;
	}
}
function updateProgress(evt) {
    // evt is an ProgressEvent.
    if (evt.lengthComputable) {
      var percentLoaded = Math.round((evt.loaded / evt.total) * 100);
      // Increase the progress bar length.
      if (percentLoaded < 100) {
        progress.style.width = percentLoaded + '%';
        progress.textContent = percentLoaded + '%';
     } else {
     }
   }
 }

function errorHandler(evt) {
    switch(evt.target.error.code) {
      case evt.target.error.NOT_FOUND_ERR:
        alert('File Not Found!');
        break;
      case evt.target.error.NOT_READABLE_ERR:
        alert('File is not readable');
        break;
      case evt.target.error.ABORT_ERR:
        break; // noop
      default:
        alert('An error occurred reading this file.');
    };
  }

function imageClick(element) {
	editImageForm.style.display = 'block';
	//editImageNameInput.value = element.id;
	var imageNameArr = element.id.split('.');
	imageNameParagraph.innerHTML = imageNameArr[0];
	imageExtensionSpan.innerHTML = imageNameArr[1];
}

function todayDateString() {
	var today = new Date();
	var dd = today.getDate();
	var mm = today.getMonth()+1; //January is 0!
	var yyyy = today.getFullYear();
	if(dd<10) {
	    dd = '0'+dd
	}

	if(mm<10) {
	    mm = '0'+mm
	}

	today = mm + '/' + dd + '/' + yyyy;

	return today;
}

function sendImage(imageData) {
	var sendResult = true;
	var xhr = new XMLHttpRequest();
    xhr.open("post","/myphotos/addphotos.htm/add.htm?uuid="
								+ addPhotosUserIDInput.value
								+ "&key="
								+ addPhotosUserKeyInput.value);
    xhr.setRequestHeader('Content-Type', 'application/json');
    xhr.send(imageData);
    xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
       if(xhr.status == 202) {
         stringValue = xhr.responseText;
         console.log(stringValue);
         	if(stringValue == "1") {
         		sendResult = true;
         	}
           }
        }
    };
	return sendResult;
}
function tempObjectTest(array) {
	for(var i = 0; i < array.length; i++ ){
		console.log(array[i].name);
		console.log(array[i].description);
		console.log(array[i].date);
		console.log(array[i].binary);
	}
}

function setPreventState(){
	 photosArray.length = 0;
	 displayPhotosOutput.innerHTML = "";
	 choosePhotosBtn.value = "";
	 sendButton.disabled = true;
	 cancelButton.disabled = true;
	 photosPreviewArea.style.display = 'none';
}

function exitFromAddPhotos() {
	console.log("Exit...");
	var xhrExitFromAddPhotos = new XMLHttpRequest();
	xhrExitFromAddPhotos.open("get","/myphotos/addphotos.htm/exitfromaddphotos.htm?uuid="
			+ addPhotosUserIDInput.value
			+ "&key="
			+ addPhotosUserKeyInput.value);
	xhrExitFromAddPhotos.setRequestHeader('Content-Type', 'text/html');
	xhrExitFromAddPhotos.send();
	xhrExitFromAddPhotos.onreadystatechange = function() {
    if (xhrExitFromAddPhotos.readyState == 4) {
       if(xhrExitFromAddPhotos.status == 202) {
    	    console.log("Exit");
           }
        }
    };
}
