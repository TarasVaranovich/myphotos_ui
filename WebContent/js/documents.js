var userNameSpan = document.getElementById("userName");
var documentsUserKeyInput = document.getElementById("documentsUserKeyInput");
var documentsUserIDInput = document.getElementById("documentsUserID");
var documentContents = document.getElementsByClassName("documentContent");
var documentNames = Array.prototype.slice.call(document.getElementsByClassName("documentName"));
var documentDates = document.getElementsByClassName("documentDate");
var documentIDs = document.getElementsByClassName("documentId");
var documentWraps = document.getElementsByClassName("pdfpageEmbed");
var exitBtn = document.getElementById("exitBtn");
var previousDocButton = document.getElementById("previousDocumentBtn");
var nextDocButton = document.getElementById("nextDocumentBtn");

var deleteDocumentsButtonSwitcher = true;
var documentIDsArr = [];
var deleteDocumentsForm = document.getElementById("deleteDocumentsForm");
var deleteDocumentsYesBtn = document.getElementById("deleteDocumentsYesBtn");
var deleteDocumentsNoBtn = document.getElementById("deleteDocumentsNoBtn");

var formModal = document.getElementById("formModal");
//Preview document
var documentPreviewForm = document.getElementById("documentPreview");
var closeDocumentPreviewFormBtn = document.getElementById("closeDocumentPreview");
var documentPreviewContent = document.getElementById("currentDoc");
var openDocumentBtn = document.getElementById("openDocument");
var deleteDocumentBtn = document.getElementById("deleteDocument");
var currentDocId = document.getElementById("currentDocId");
//end Preview document
var waitAnimation = document.getElementById("waitAnimation");
var commonAlert = document.getElementById("commonAlert");

var documentsCursor = 0;

window.onload = function() {

	var xhr = new XMLHttpRequest();
    xhr.open("post","/myphotos/documents.htm/usr.htm"); // CCCC
    xhr.setRequestHeader('Content-Type', 'text/html');
    xhr.send(null);
    xhr.onreadystatechange = function() {
    if (xhr.readyState == 4) {
       if(xhr.status == 200) {
       		var stringValue = xhr.responseText;
       		documentsUserKeyInput.value = JSON.parse(stringValue).token;
       		documentsUserIDInput.value = JSON.parse(stringValue).uuid;

	       		for(var z = 0; z < documentContents.length; z++) {
	       			getDocument(z, z);
	            }
           }
        }
    };

    for(var z = 0; z < documentContents.length; z++) {

	   	 documentNames[z].addEventListener("click", function(event) {
	    		documentNameClick(this);
	    });
    }

    deleteDocumentsForm.style.display= "none";
    documentPreviewForm.style.display= "none"; //NONE BY DEFAULT
		formModal.style.display = "none"; // NONE BY DEFAULT
		waitAnimation.style.display = "none";
		commonAlert.style.display = "none";
}

nextDocButton.addEventListener("click", function(event) {

	documentsCursor = documentsCursor + 1;
	for(var i = 0; i < documentContents.length; i++) {
		getDocument(i, (documentsCursor + i));
	}
	previousDocButton.disabled = false;
});

previousDocButton.addEventListener("click", function(event) {

	documentsCursor = documentsCursor - 1;
	nextDocButton.disabled = false;
	if(documentsCursor >= 0 ) {
		if(documentsCursor == 0) {
			previousDocButton.disabled = true;
		}
		for(var k = 0; k < documentContents.length; k++) {
			documentContents[k].style.display = 'inline-block'; ////
			documentNames[k].style.display = 'inline-block';
			documentDates[k].style.display = 'inline-block';
			documentIDs[k].style.display = 'inline-block';
			documentWraps[k].style.display = 'inline-block';
		}
		for(var i = 0; i < documentContents.length; i++) {
			getDocument(i, (documentsCursor + i));
		}
	}

});

deleteDocumentsYesBtn.addEventListener("click", function(event) {

	deleteDocuments(documentIDsArr);
	setPreventState();
});

deleteDocumentsNoBtn.addEventListener("click", function(event) {

	setPreventState();
});

exitBtn.addEventListener("click", function(event) {

	exitFromDocuments();
	window.location.href = "/myphotos/welcome.htm";

});
openDocumentBtn.addEventListener("click", function(event) {

	window.open(documentPreviewContent.src);

});
deleteDocumentBtn.addEventListener("click", function(event) {

	documentIDsArr.push(currentDocId.value);
	deleteDocumentsForm.style.display = "block";

});
closeDocumentPreviewFormBtn.addEventListener("click", function(event) {

	documentPreviewForm.style.display= "none";
	formModal.style.display = "none";
	currentDocId.value = null;
	documentPreviewContent.src = null;

});

function getDocument(z, k){

	var xhrDocument = new XMLHttpRequest();
	xhrDocument.open("get","/myphotos/documents.htm/getdocuments.htm?uuid="
										+ documentsUserIDInput.value
										+ "&key="
										+ documentsUserKeyInput.value);
	xhrDocument.setRequestHeader('Content-Type', 'application/json');
	xhrDocument.setRequestHeader('documentnumber', k);
	xhrDocument.send();
	xhrDocument.onreadystatechange = function() {
    if (xhrDocument.readyState == 4) {
       if(xhrDocument.status == 202) {
         var stringValueDocument = xhrDocument.responseText;
         if(stringValueDocument!= "") {
	         documentContents[z].src = JSON.parse(stringValueDocument).docbin;
	         documentNames[z].innerHTML = JSON.parse(stringValueDocument).docname;
	         documentDates[z].innerHTML = JSON.parse(stringValueDocument).docdate;
	         documentIDs[z].value = JSON.parse(stringValueDocument).docid;
         } else {
        	 console.log("Empty document");
        	 documentContents[z].style.display = 'none';
        	 documentNames[z].style.display = 'none';
        	 documentDates[z].style.display = 'none';
        	 documentIDs[z].style.display = 'none';
        	 documentWraps[z].style.display = 'none';
        	 nextDocButton.disabled = true;
         }
           }
        }
    };
}

function deleteDocuments(IDS) {
	var deleteObj = new Object();
	deleteObj.documentIDS = IDS;
	var deleteData = JSON.stringify(deleteObj);
	var xhrDelete = new XMLHttpRequest();
	xhrDelete.open("post","/myphotos/documents.htm/deletedocuments.htm?uuid="
										+ documentsUserIDInput.value
										+ "&key="
										+ documentsUserKeyInput.value);
	xhrDelete.setRequestHeader('Content-Type', 'application/json');
	xhrDelete.send(deleteData);
	xhrDelete.onreadystatechange = function() {
    if (xhrDelete.readyState == 4) {
       if(xhrDelete.status == 200) {
         var stringValueDelete = xhrDelete.responseText;
         if(stringValueDelete == "true") {
        	 	refreshDocuments();
			}
           }
        }
    };
}

function documentNameClick(documentName) {

	var number = documentNames.indexOf(documentName);

	documentPreviewForm.style.display= "block";
	formModal.style.display = "block";
	currentDocId.value = documentIDs[number].value;
	documentPreviewContent.src = documentContents[number].src;
}

function setPreventState() {
	documentIDsArr.length = 0;
	deleteDocumentsForm.style.display="none";
	checkBoxesVisible = false;
}

function refreshDocuments() {
	nextDocButton.disabled = false;
	for(var i = 0; i < documentContents.length; i++) {
		documentContents[i].style.display = 'inline-block';
   	 	documentNames[i].style.display = 'inline-block';
   	 	documentDates[i].style.display = 'inline-block';
   	 	documentIDs[i].style.display = 'inline-block';
   	 	documentWraps[i].style.display = 'inline-block';
   	 	getDocument(i, (documentsCursor + i));
 	}
}

function exitFromDocuments() {
	console.log("Exit...");
	var xhrExitFromDocuments = new XMLHttpRequest();
	xhrExitFromDocuments.open("get","/myphotos/documents.htm/exitfromdocuments.htm?uuid="
			+ documentsUserIDInput.value
			+ "&key="
			+ documentsUserKeyInput.value);
	xhrExitFromDocuments.setRequestHeader('Content-Type', 'text/html');
	xhrExitFromDocuments.send();
	xhrExitFromDocuments.onreadystatechange = function() {
    if (xhrExitFromDocuments.readyState == 4) {
       if(xhrExitFromDocuments.status == 202) {
    	    console.log("Exit");
           }
        }
    };
}
