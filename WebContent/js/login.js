var nameInputLogin = document.getElementById("loginUser");
var passwordInputLogin = document.getElementById("loginPassword");
var loginBtn = document.getElementById("loginBtn");
var loginKeyInput = document.getElementById("loginHiddenKey");
var loginUserIDInput = document.getElementById("loginUserID");
var gamalPKeyInputLogin = document.getElementById("gamalPKeyLogin");
var gamalGKeyInputLogin = document.getElementById("gamalGKeyLogin");
var gamalYKeyInputLogin = document.getElementById("gamalYKeyLogin");
var loginOutput = document.getElementById("loginResult");
var waitAnimation = document.getElementById("waitAnimation");
var commonAlert = document.getElementById("commonAlert");

loginBtn.addEventListener("click", function(event) {
	var userName = nameInputLogin.value;
	var password = passwordInputLogin.value;

	if((userName == "") || (password == "")){

        alert("Empty data!");

      } else {

    	  var userObj = new Object();
    	  userObj.name = userName;
    	  userObj.password = password; // encript
    	  var encryptResult = "";
    	  var encodeChar = "";
    	  var gPf = gamalPKeyInputLogin.value;
		  var gGf = gamalGKeyInputLogin.value;
		  var gYf = gamalYKeyInputLogin.value;
		  //userName
		  for (var i = 0; i < userName.length; i++) {

	  		    var charCode = userName.charCodeAt(i);

	  		    encodeChar =  encryptCode(gPf, gGf, gYf, charCode) + ",";

	  		    encryptResult += encodeChar;
		  }
		  userObj.name = encryptResult;
		  encryptResult = "";
		  //password
    	  for (var i = 0; i < password.length; i++) {

    		    var charCode = password.charCodeAt(i);

    		    encodeChar =  encryptCode(gPf, gGf, gYf, charCode) + ",";

    		    encryptResult += encodeChar;
    	  }
    	  //console.log(result);
    	  userObj.password = encryptResult;
    	  //--------
    	  var userData = JSON.stringify(userObj);

    	  var xhr = new XMLHttpRequest();
          xhr.open("post","/myphotos/login.htm/usr.htm?uuid="
								+ loginUserIDInput.value
								+ "&key="
								+ loginKeyInput.value);
          xhr.setRequestHeader('Content-Type', 'application/json');
          xhr.send(userData);
          //console.log(userData);
          xhr.onreadystatechange = function() {
          if (xhr.readyState == 4) {
             if(xhr.status == 202) {
               stringValue = xhr.responseText;
               console.log(stringValue);
               // new changet credentials
               var result = JSON.parse(stringValue).result;
	               if(result == 1) {
	            	   var tokenRes = JSON.parse(stringValue).key;
	                   var uuidRes = JSON.parse(stringValue).uuid;
	            	   window.location.href = "/myphotos/photos.htm?uuid="
	            		   						+ uuidRes
	            		   						+ "&key="
	            		   						+ tokenRes;
	               } else {
									 loginOutput.style.display = 'block';
	            	   loginOutput.innerHTML = result;
	               }
                 }
              }
          };
      }
});



window.onload = function() {

	/*var currentDate = new Date().getTime();
	var navigatorAgent = navigator.userAgent;
	var clientObj = new Object();
	clientObj.time = currentDate;
	clientObj.agent = navigatorAgent;
	var clientData = JSON.stringify(clientObj);*/

	var xhr = new XMLHttpRequest();
     xhr.open("post","/myphotos/login.htm");
     xhr.setRequestHeader('Content-Type', 'application/json');
     xhr.send(0); // null fixed
     xhr.onreadystatechange = function() {
     if (xhr.readyState == 4) {
        if(xhr.status == 202) {
        		var stringValue = xhr.responseText;
        		loginKeyInput.value = JSON.parse(stringValue).key;
        		loginUserIDInput.value = JSON.parse(stringValue).uuid;
        		var gamalP = JSON.parse(stringValue).gamalP;
        		var gamalG = JSON.parse(stringValue).gamalG;
        		var gamalY = JSON.parse(stringValue).gamalY;
        		//loginKeyInput.value = token;
        		//loginUserIDInput.value = JSON.parse(stringValue).uuid;
        		gamalPKeyInputLogin.value = gamalP;
        		gamalGKeyInputLogin.value = gamalG;
        		gamalYKeyInputLogin.value = gamalY;
            }
         }
     };
     loginOutput.style.display = 'none';
		 waitAnimation.style.display = 'none';
		 commonAlert.style.display = 'none';
}

function encryptCode(gP, gG, gY, charCode) {

    var gKd = 1 + Math.random()*(200);
    gK = Math.floor(gKd);

    var gPBI = new BigInteger(gP);
    var gGBI = new BigInteger(gG);
    var gYBI = new BigInteger(gY);
    var gKBI = new BigInteger(gK);
    var X = new BigInteger(charCode);

    var gA = gGBI.modPow(gKBI, gPBI);
    var gB =X.multiply(gYBI.modPow(gKBI, gPBI)).remainder(gPBI);

    var gAstr = gA.toString();
    var gBstr = gB.toString();

    var resultStr = gAstr + ":" + gBstr;

    return resultStr;
}
