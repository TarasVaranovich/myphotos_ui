var nameInput = document.getElementById("registerUser");
var passwordInput = document.getElementById("registerPassword");
var passwordConfirmInput = document.getElementById("registerPasswordConfirm");
var registerBtn = document.getElementById("registerBtn");
var registerKeyInput = document.getElementById("registerHiddenKey");
var registerUserIDInput = document.getElementById("registerUserID");
var gamalPKeyInput = document.getElementById("gamalPKey");
var gamalGKeyInput = document.getElementById("gamalGKey");
var gamalYKeyInput = document.getElementById("gamalYKey");
var resultOutput = document.getElementById("queryResult");
var waitAnimation = document.getElementById("waitAnimation");
var commonAlert = document.getElementById("commonAlert");

registerBtn.addEventListener("click", function(event) {
	var userName = nameInput.value;
	var password = passwordInput.value;
	var passwordConfirm = passwordConfirmInput.value;

	if((userName == "") || (password == "")){

        alert("Empty data!");

      } else {

    	  var userObj = new Object();
    	  userObj.name = userName;
    	  userObj.password = password; // encript
    	  var encryptResult = "";
    	  var encodeChar = "";
    	  var gPf = gamalPKeyInput.value;
		  var gGf = gamalGKeyInput.value;
		  var gYf = gamalYKeyInput.value;
		  //userName
		  for (var i = 0; i < userName.length; i++) {

	  		    var charCode = userName.charCodeAt(i);

	  		    encodeChar =  encryptCode(gPf, gGf, gYf, charCode) + ",";

	  		    encryptResult += encodeChar;
		  }
		  userObj.name = encryptResult;
		  encryptResult = "";
		  //password
    	  for (var i = 0; i < password.length; i++) {

    		    var charCode = password.charCodeAt(i);

    		    encodeChar =  encryptCode(gPf, gGf, gYf, charCode) + ",";

    		    encryptResult += encodeChar;
    	  }
    	  //console.log(result);
    	  userObj.password = encryptResult;
    	  //--------
    	  var userData = JSON.stringify(userObj);

    	  var xhr = new XMLHttpRequest();
          xhr.open("post","/myphotos/registration.htm/usr.htm?uuid="
								+ registerUserIDInput.value
								+ "&key="
								+ registerKeyInput.value);
          xhr.setRequestHeader('Content-Type', 'application/json');
          xhr.send(userData);
          xhr.onreadystatechange = function() {
          if (xhr.readyState == 4) {
             if(xhr.status == 202) {
               stringValue = xhr.responseText;
               console.log(stringValue);
               var result = JSON.parse(stringValue).result;

               if(result == 1) {
            	   var tokenRes = JSON.parse(stringValue).key;
                   var uuidRes = JSON.parse(stringValue).uuid;
            	   window.location.href = "/myphotos/photos.htm?uuid="
  											+ uuidRes
  											+ "&key="
  											+ tokenRes;
               } else {
								 resultOutput.style.display = 'block';
            	   resultOutput.innerHTML = result;
               }
                 }
              }
          };
      }
});


window.onload = function() {

	var currentDate = new Date().getTime();
	var navigatorAgent = navigator.userAgent;
	var clientObj = new Object();
	clientObj.time = currentDate;
	clientObj.agent = navigatorAgent;
	var clientData = JSON.stringify(clientObj);

	var xhr = new XMLHttpRequest();
     xhr.open("post","/myphotos/registration.htm");
     xhr.setRequestHeader('Content-Type', 'application/json');
     xhr.send(clientData);
     xhr.onreadystatechange = function() {
     if (xhr.readyState == 4) {
        if(xhr.status == 202) {
        		var stringValue = xhr.responseText;
        		var token = JSON.parse(stringValue).key;
        		var gamalP = JSON.parse(stringValue).gamalP;
        		var gamalG = JSON.parse(stringValue).gamalG;
        		var gamalY = JSON.parse(stringValue).gamalY;
        		registerKeyInput.value = token;
        		registerUserIDInput.value = JSON.parse(stringValue).uuid;
        		gamalPKeyInput.value = gamalP;
        		gamalGKeyInput.value = gamalG;
        		gamalYKeyInput.value = gamalY;
            }
         }
     };
     resultOutput.style.display = 'none';
		 waitAnimation.style.display = 'none';
		 commonAlert.style.display = 'none';
}

function encryptCode(gP, gG, gY, charCode) {

    var gKd = 1 + Math.random()*(200);
    gK = Math.floor(gKd);

    var gPBI = new BigInteger(gP);
    var gGBI = new BigInteger(gG);
    var gYBI = new BigInteger(gY);
    var gKBI = new BigInteger(gK);
    var X = new BigInteger(charCode);

    var gA = gGBI.modPow(gKBI, gPBI);
    var gB =X.multiply(gYBI.modPow(gKBI, gPBI)).remainder(gPBI);

    var gAstr = gA.toString();
    var gBstr = gB.toString();

    var resultStr = gAstr + ":" + gBstr;

    return resultStr;
}
